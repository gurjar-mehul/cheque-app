class CreateCheques < ActiveRecord::Migration[5.2]
  def change
    create_table :cheques do |t|
      t.string :recipient_name
      t.date :payment_date
      t.float :amount

      t.timestamps
    end
  end
end
