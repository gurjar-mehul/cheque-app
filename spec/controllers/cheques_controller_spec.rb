require 'rails_helper'

RSpec.configure do |c|
  c.use_transactional_examples = false
  c.order = "defined"
end
   
RSpec.describe ChequesController, type: :controller do
  let(:cheque_one) { cheques(:cheque_one) }

  describe 'GET#Index' do
    context 'success 200' do
      before { get :index }
      it { expect(response).to have_http_status(:ok) }
      it { expect(assigns(:cheques)).to include(cheques(:cheque_one)) }
    end

    context 'success 200 with params recipient_name' do
      before { get :index, params: { recipient_name: 'Test data' } }
      it { expect(response).to have_http_status(:ok) }
      it { expect(assigns(:cheques)).to include(cheques(:cheque_one)) }
    end
  end
end
