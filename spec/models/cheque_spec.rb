require 'rails_helper'

RSpec.describe Cheque, type: :model do
  describe 'Validations' do
    context 'Presence' do
      it { should validate_presence_of :recipient_name }
      it { should validate_presence_of :payment_date }
      it { should validate_presence_of :amount }
    end
  end
end
