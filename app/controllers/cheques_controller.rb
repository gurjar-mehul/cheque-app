class ChequesController < ApplicationController
  before_action :set_cheque, only: :show

  # List of cheques
  # ====URL
  #    /cheques [GET]
  # ====Filter Parameters
  #   recipient_name
  def index
    if params.dig(:recipient_name)
      @cheques = Cheque.by_name(params[:recipient_name]).order(created_at: :desc)
    else
      @cheques = Cheque.order(created_at: :desc)
    end
  end

  # View cheque
  # ====URL
  #    /cheques/:ID [GET]
  def show
  end

  # GET /cheques/new
  def new
    @cheque = Cheque.new
  end


  # Create a cheque
  # ====URL
  #    /cheques [POST]
  # ====Parameters
  #   cheque[recipient_name]
  #   cheque[payment_date]
  #   cheque[amount]
  def create
    @cheque = Cheque.new(cheque_params)
    if @cheque.save
      redirect_to @cheque, notice: 'Cheque was successfully created.'
    else
      render :new
    end
  end

  private

    def set_cheque
      @cheque = Cheque.find(params[:id])
    end

    def cheque_params
      params.require(:cheque).permit(:recipient_name, :payment_date, :amount)
    end
end
