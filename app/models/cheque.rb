class Cheque < ApplicationRecord
  validates :recipient_name, :payment_date, :amount, presence: true
  validates :amount, numericality: true, allow_blank: true

  scope :by_name, -> (recipient_name) { where(recipient_name: recipient_name ) }
end
