Rails.application.routes.draw do
  root to: 'cheques#index'
  resources :cheques, only: [:index, :new, :create, :show]
end
