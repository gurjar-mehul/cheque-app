# README

#Cheque App#

We need an application to print virtual bank cheques. The application takes in the recipient name, a date and a nominal value, and generates a virtual cheque (on screen) with the proper string representation of the value, eg. when 1,303.48 is entered, convert to “One Thousand Three Hundred and Three dollars and Forty-Eight cents”.

##User Stories##

 - 1.1 – Creating a new cheque requires the operator to enter a value, a payment date and a recipient name. Entering valid details will trigger the application to (a) save the cheque generated and (b) display the generated cheque, as per 1.4.

 - 1.2 – List of all cheques saved is available and shows a the date, recipient name and value as well as a link to view.

 - 1.3 – Filtering the list is possible by clicking on a recipient name, which results in showing the list of cheque as 1.2, but for a selected recipient only.

 - 1.4 – Viewing a cheque allows for the operator to view the cheque as saved and generated out the system, including (but not limited to!) the numeric to string representation conversion.

##The technologies Used:##

###Web API:###

 - Ruby 2.6
 - Rails 5.2.3
 - SQLite[Dev, Test], Postgres[production]

###Client App:###

 - Rails views with bootstrap

###Testing:###

 - RSpec (for back-end tests)

###Deployment:###

 - Heroku

###Version Control:###

 - Git

##Installation:##

 - Make sure your sqlite3 server is running.
 - `rails db:create`
 - `rails db:migrate`
 - `rails server`

##Application Structure:##

 - MVC architecture used to build app.
 - BDD test cases written for app using Rspec.

##Directory structures for web api:##

 - app/controllers → Handle browser request

 - app/models → Validation, Scope, Business logic

 - app/views → User representation view


##Tests:##


 - spec/controllers → Tests for controllers

 - spec/models → Tests for models

 - spec/fixtures → Static data for testing

##Run test cases:##

 - `rspec spec/controllers/cheques_controller_spec.rb -f d`

 - `rspec spec/models/cheque_spec.rb -f d`
